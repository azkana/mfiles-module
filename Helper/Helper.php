<?php

use GuzzleHttp\Client;

/**
 * Get Property Detail
 * @param int $id
 * @return string
 */

function mfilesGetPropertyDetail($id)
{
    $client     = new Client();
        $endPoint   = env('MFILES_BASE_URL') . 'structure/properties/' . $id;
        $headers    = [
                'Accept'        => 'application/json',
                'X-Username'    => env('MFILES_USERNAME'),
                'X-Password'    => env('MFILES_PASSWORD'),
                'X-Vault'       => '{' . env('MFILES_VAULT_ID') . '}'
            ];
        $response       = $client->request('GET', $endPoint, [ 'headers' => $headers]);
        $result         = json_decode($response->getbody()->getContents(), true);
        return $result['Name'];
}

if(! function_exists('__getDetailPropertyDef')) {
    function __getDetailPropertyDef($data=array(), $id, $dataType) {
        $dataS = array_column($data, 'TypedValue', 'PropertyDef');
        switch ($dataType) {
            case 1:
                $result = $dataS[$id]['DisplayValue'];
                break;
            case 5:
                $result = dateFormatDmy($dataS[$id]['DisplayValue']);
                break;
            case 7:
                $result = dateFormatDmyHi($dataS[$id]['DisplayValue']);
                break;
            case 9:
                $result = $dataS[$id]['Lookup']['DisplayValue'];
                break;
            case 10:
                $result = $dataS[$id]['DisplayValue'];
                break;
            case 13:
                $result = $dataS[$id]['DisplayValue'];
                break;
            default:
                $result = 'N/A';
                break;
        }
        return $result;
    }
}

if(! function_exists('__getStateDetail')) {
    function __getStateDetail($state)
    {
        switch ($state) {
            case true:
                $result = 'Ada';
                break;
            case false:
                $result = 'Tidak Ada';
                break;
            default:
                $result = 'Tidak Ada';
                break;
        }

        return $result;
    }
}