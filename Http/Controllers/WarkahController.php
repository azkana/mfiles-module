<?php

namespace Modules\Mfiles\Http\Controllers;

use Exception;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Mfiles\Entities\Warkah;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use Modules\Mfiles\Exports\WarkahExportToExcel;

class WarkahController extends Controller
{
    protected $pageTitle;
    protected $baseUrl;
    protected $username;
    protected $password;
    protected $vaultID;

    protected $client;
    protected $headers;

    public function __construct()
    {
        $this->pageTitle = 'Warkah';
        $this->baseUrl      = config('mfiles.base_url');
        $this->username     = config('mfiles.username');
        $this->password     = config('mfiles.password');
        $this->vaultID      = config('mfiles.vault_id');

        $this->client       = new Client();
        $this->headers      = [
            'Accept'        => 'application/json',
            'X-Username'    => $this->username,
            'X-Password'    => $this->password,
            'X-Vault'       => $this->vaultID
        ];
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $params['pageTitle']    = $this->pageTitle;
        return view('mfiles::warkah.index', $params);
    }

    public function indexData(Request $request)
    {
        if($request->ajax()) {
            $data = new Warkah();
            $data = $data->orderBy('last_modified', 'desc');
            $data = $data->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row) {
                    $actionBtn = 
                    '
                        <div class="btn-group">
                            <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Action</span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li>
                                    <a href="#" onclick="showDetail(\''.$row->id.'\');return false;"><i class="fa fa-eye"></i> Detail</a>
                                </li>
                            </ul>
                        </div>
                    ';
                    return $actionBtn;
                })
                ->rawColumns([
                    'action'
                ])
                ->make(true);
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request)
    {
        $params['title']    = $this->pageTitle;
        $params['data']     = Warkah::findOrFail($request->id);
        return view('mfiles::warkah.show', $params);
    }

    public function sync()
    {
        try {

            ini_set('memory_limit','256M');
            ini_set('max_execution_time','0');
            $endPoint   = $this->baseUrl . 'views/V101/L9/items?limit=20000';
            $response   = $this->client->request('GET', $endPoint, [ 'headers' => $this->headers]);
            $result         = json_decode($response->getbody()->getContents(), true);
            
            foreach($result['Items'] as $item) {
                if($item['FolderContentItemType'] == 4) {
                    $data['docTitle']   = $item['ObjectVersion']['Title'];
                    $data['docLastModUtc']  = $item['ObjectVersion']['LastModifiedUtc'];
                    $data['docID']      = $docID        = $item['ObjectVersion']['ObjVer']['ID'];
                    $data['docVersion'] = $docVersion   = $item['ObjectVersion']['ObjVer']['Version'];
                    $data['docType']    = $docType      = $item['ObjectVersion']['ObjVer']['Type'];
                    $data['fileSize']   = $item['ObjectVersion']['Files'][0]['Size'];
                    $data['fileExt']    = $item['ObjectVersion']['Files'][0]['Extension'];

                    $data['docProperties']  = array_reduce($this->getDocumentProperties($docType, $docID, $docVersion), 'array_merge', []);

                    if($data['docProperties']['supportingDocumentType'] == 'Warkah') {
                        $dataWrkh['doc_id']     = $data['docID'];
                        $dataWrkh['doc_title']  = $data['docTitle'];
                        $dataWrkh['doc_version']= $data['docVersion'];
                        $dataWrkh['doc_type']   = $data['docType'];
                        $dataWrkh['file_size']  = $data['fileSize'];
                        $dataWrkh['file_ext']   = $data['fileExt'];

                        $dataWrkh['nomor']      = $data['docProperties']['nomorDokumen'];
                        $dataWrkh['name']       = $data['docProperties']['subjectName'];
                        $dataWrkh['jenis']      = $data['docProperties']['jenisDokumen'];
                        $dataWrkh['luas']       = (double) cleanNominal($data['docProperties']['luasDokumen']);
                        $dataWrkh['desa']       = capitalize($data['docProperties']['desa']);
                        $dataWrkh['kecamatan']  = capitalize($data['docProperties']['kecamatan']);
                        $dataWrkh['kabupaten']  = capitalize($data['docProperties']['kabupaten']);
                        $dataWrkh['sph_no']     = $data['docProperties']['nomorSPH'];
                        $dataWrkh['sph_nm']     = $data['docProperties']['namaSPH'];
                        $dataWrkh['sph_dt']     = dateFormatYmd($data['docProperties']['tanggalSPH']);
                        $dataWrkh['sph_ls']     = (double) cleanNominal($data['docProperties']['luasSPH']);
                        $dataWrkh['ktp']        = $this->convertState($data['docProperties']['kTP']);
                        $dataWrkh['kk']         = $this->convertState($data['docProperties']['kartuKeluarga']);
                        $dataWrkh['sk_sengketa']= $this->convertState($data['docProperties']['suratKeteranganSengketa']);
                        $dataWrkh['sk_waris']   = $this->convertState($data['docProperties']['suratKeteranganWaris']);
                        $dataWrkh['k_waris']    = $this->convertState($data['docProperties']['kuasaWaris']);
                        $dataWrkh['k_jual']     = $this->convertState($data['docProperties']['kuasaJual']);
                        $dataWrkh['pbb']        = $this->convertState($data['docProperties']['pBB']);

                        $dataWrkh['last_sync']  = Carbon::now();
                        $dataWrkh['last_sync_by']= Auth::user()->id;
                        $dataWrkh['created']    = dateFormatYmdHis($data['docProperties']['created']);
                        $dataWrkh['created_by'] = $data['docProperties']['createdBy'];
                        $dataWrkh['last_modified']= dateFormatYmdHis($data['docProperties']['lastModified']);
                        $dataWrkh['last_modified_by'] = $data['docProperties']['lastModifiedBy'];

                        $cekWrkhExist = Warkah::where('doc_id', $data['docID']);
                
                        if($cekWrkhExist->count() > 0) {
                            $lastModified = dateFormatYmdHis($data['docProperties']['lastModified']);
                            if($cekWrkhExist->where('last_modified', '!=', $lastModified)->count() > 0) {
                                $cekWrkhExist->update($dataWrkh);
                            }
                        } else {
                            Warkah::create($dataWrkh);
                        }
                    }
                }
            }

            return response()->json([
                'message' => trans('message.success_save')
            ]);

        } catch(Exception $e) {

            return response()->json([
                'message' => trans('message.error_save')
            ]);
        }
    }

    public function exportToExcel()
    {
        
        $data = Warkah::orderBy('last_modified', 'desc')->get();

        return Excel::download(new WarkahExportToExcel($data), 'warkah_'.date('Y-m-d').'.xlsx');
        
    }

    private function getDocumentProperties($type, $id, $version) {
        $endPoint   = $this->baseUrl . 'objects/properties;' . $type . '/' . $id . '/' . $version;
        $response   = $this->client->request('GET', $endPoint, [ 'headers' => $this->headers]);
        $result     = json_decode($response->getbody()->getContents(), true);
        $resultArray= [];
        foreach($result[0] as $item) {
            $arrayData = array(
                camel_case($this->getPropertyDefDetail($item['PropertyDef'])) => $item['Value']['DisplayValue']
            );
            array_push($resultArray, $arrayData);
        }
        return array_merge($resultArray);
    }

    private function getPropertyDefDetail($id) {
        $endPoint   = $this->baseUrl . 'structure/properties/' . $id;
        $response       = $this->client->request('GET', $endPoint, [ 'headers' => $this->headers]);
        $result         = json_decode($response->getbody()->getContents(), true);
        return $result['Name'];
    }

    private function convertState($state)
    {
        switch ($state) {
            case 'Ada':
                $result = true;
                break;
            case 'Tidak Ada':
                $result = false;
                break;
            default:
                $result = false;
                break;
        }

        return $result;
    }

}
