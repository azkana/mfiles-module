<?php

namespace Modules\Mfiles\Http\Controllers;

use Exception;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Mfiles\Entities\Sertipikat;
use Modules\Mfiles\Exports\DocumentSertipikatExportToExcel;
use Yajra\DataTables\Facades\DataTables;

class SertipikatController extends Controller
{
    protected $pageTitle;
    protected $baseUrl;
    protected $username;
    protected $password;
    protected $vaultID;

    protected $client;
    protected $headers;

    public function __construct()
    {
        $this->pageTitle = 'Sertipikat';
        $this->baseUrl      = config('mfiles.base_url');
        $this->username     = config('mfiles.username');
        $this->password     = config('mfiles.password');
        $this->vaultID      = config('mfiles.vault_id');

        $this->client       = new Client();
        $this->headers      = [
            'Accept'        => 'application/json',
            'X-Username'    => $this->username,
            'X-Password'    => $this->password,
            'X-Vault'       => $this->vaultID
        ];
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $params['pageTitle']    = $this->pageTitle;
        return view('mfiles::sertipikat.index', $params);
    }

    public function indexData(Request $request)
    {
        if($request->ajax()) {
            $data = new Sertipikat();
            $data = $data->orderBy('last_modified', 'desc');
            $data = $data->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('alamat', function($row) {
                    return capitalize(strtolower($row->alamat));
                })
                ->addColumn('action', function($row) {
                    $actionBtn = 
                    '
                        <div class="btn-group">
                            <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Action</span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li>
                                    <a href="#" onclick="showDetail(\''.$row->id.'\');return false;"><i class="fa fa-eye"></i> Detail</a>
                                </li>
                            </ul>
                        </div>
                    ';
                    return $actionBtn;
                })
                ->rawColumns([
                    'action', 'alamat'
                ])
                ->make(true);
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request)
    {
        $params['title']    = $this->pageTitle;
        $params['data']     = Sertipikat::findOrFail($request->id);
        return view('mfiles::sertipikat.show', $params);
    }

    public function sync()
    {
        try {
            ini_set('memory_limit','256M');
            ini_set('max_execution_time','0');
            $endPoint   = $this->baseUrl . 'views/V101/L17/items?limit=20000';
            $response   = $this->client->request('GET', $endPoint, [ 'headers' => $this->headers]);
            $result         = json_decode($response->getbody()->getContents(), true);
            
            foreach($result['Items'] as $item) {

                if($item['FolderContentItemType'] == 4) {
                    $data['docTitle']   = $item['ObjectVersion']['Title'];
                    $data['docLastModUtc']  = $item['ObjectVersion']['LastModifiedUtc'];
                    $data['docID']      = $docID        = $item['ObjectVersion']['ObjVer']['ID'];
                    $data['docVersion'] = $docVersion   = $item['ObjectVersion']['ObjVer']['Version'];
                    $data['docType']    = $docType      = $item['ObjectVersion']['ObjVer']['Type'];
                    $data['fileSize']   = $item['ObjectVersion']['Files'][0]['Size'];
                    $data['fileExt']    = $item['ObjectVersion']['Files'][0]['Extension'];

                    $data['docProperties']  = array_reduce($this->getDocumentProperties($docType, $docID, $docVersion), 'array_merge', []);

                    $pemegangHak = $data['docProperties']['namaPemegangHak'] != 'Lain-lain'? $data['docProperties']['namaPemegangHak'] : $data['docProperties']['namaPemilikAwal'];

                    $dataSrtp['doc_id']     = $data['docID'];
                    $dataSrtp['doc_title']  = $data['docTitle'];
                    $dataSrtp['doc_version']= $data['docVersion'];
                    $dataSrtp['doc_type']   = $data['docType'];
                    $dataSrtp['file_size']  = $data['fileSize'];
                    $dataSrtp['file_ext']   = $data['fileExt'];

                    $dataSrtp['index']      = $data['docProperties']['indexNumber'];
                    $dataSrtp['nomor']      = str_pad($data['docProperties']['nomorSertifikat'], 5, '0', STR_PAD_LEFT);
                    $dataSrtp['tgl_buat']   = dateFormatYmd($data['docProperties']['tanggalSertifikat']);
                    $dataSrtp['tgl_akhir']  = dateFormatYmd($data['docProperties']['tanggalValid']);
                    $dataSrtp['jenis']      = $data['docProperties']['jenisSertifikat'];
                    $dataSrtp['tipe']       = $data['docProperties']['tipeSertifikat'];
                    $dataSrtp['luas']       = cleanNominal($data['docProperties']['luas']);
                    $dataSrtp['alamat']     = capitalize($data['docProperties']['alamatObjek']);
                    $dataSrtp['pemegang_hak']= $pemegangHak;
                    $dataSrtp['bus_unit']   = $data['docProperties']['businessUnit'];
                    $dataSrtp['doos']       = $data['docProperties']['doos'];
                    $dataSrtp['rak']        = $data['docProperties']['rak'];
                    $dataSrtp['bast']       = $data['docProperties']['bAST'];
                    $dataSrtp['sup_doc']    = $data['docProperties']['supportingDocument'];
                    $dataSrtp['last_sync']  = Carbon::now();
                    $dataSrtp['last_sync_by']= Auth::user()->id;
                    $dataSrtp['created']    = dateFormatYmdHis($data['docProperties']['created']);
                    $dataSrtp['created_by'] = $data['docProperties']['createdBy'];
                    $dataSrtp['last_modified']= dateFormatYmdHis($data['docProperties']['lastModified']);
                    $dataSrtp['last_modified_by'] = $data['docProperties']['lastModifiedBy'];

                    $cekSrtpExist = Sertipikat::where('doc_id', $data['docID']);
                    if($cekSrtpExist->count() > 0) {
                        $lastModified = dateFormatYmdHis($data['docProperties']['lastModified']);
                        if($cekSrtpExist->where('last_modified', '!=', $lastModified)->count() > 0) {
                            $cekSrtpExist->update($dataSrtp);
                        }
                    } else {
                        Sertipikat::create($dataSrtp);
                    }
                }
            }

            return response()->json([
                'message' => trans('message.success_save')
            ]);
        
        } catch(Exception $e) {

            return response()->json([
                'message' => trans('message.error_save')
            ]);
        }
            
    }

    public function exportToExcel()
    {
        
        $data = Sertipikat::orderBy('last_modified', 'desc')->get();

        return Excel::download(new DocumentSertipikatExportToExcel($data), 'sertipikat_'.date('Y-m-d').'.xlsx');
        
    }

    private function getDocumentProperties($type, $id, $version) {
        $endPoint   = $this->baseUrl . 'objects/properties;' . $type . '/' . $id . '/' . $version;
        $response   = $this->client->request('GET', $endPoint, [ 'headers' => $this->headers]);
        $result     = json_decode($response->getbody()->getContents(), true);
        $resultArray= [];
        foreach($result[0] as $item) {
            $arrayData = array(
                camel_case($this->getPropertyDefDetail($item['PropertyDef'])) => $item['Value']['DisplayValue']
            );
            array_push($resultArray, $arrayData);
        }
        return array_merge($resultArray);
    }

    private function getPropertyDefDetail($id) {
        $endPoint   = $this->baseUrl . 'structure/properties/' . $id;
        $response       = $this->client->request('GET', $endPoint, [ 'headers' => $this->headers]);
        $result         = json_decode($response->getbody()->getContents(), true);
        return $result['Name'];
    }

    public function syncDataNull()
    {
        ini_set('memory_limit','256M');
        ini_set('max_execution_time','0');

        $srtp = Sertipikat::where('doc_id', null)->get();

        foreach($srtp as $row) {
            
            $endPoint   = $this->baseUrl . 'objects.aspx?q=' . $row->doc_title;
            $response   = $this->client->request('GET', $endPoint, [ 'headers' => $this->headers]);
            $result         = json_decode($response->getbody()->getContents(), true);
            
            if(count($result['Items']) > 0) {
                
                foreach(array_slice($result['Items'], 0, 1) as $item) {
                    
                    $data['docTitle']   = $item['Title'];
                    $data['docLastModUtc']  = $item['LastModifiedUtc'];
                    $data['docID']      = $docID        = $item['ObjVer']['ID'];
                    $data['docVersion'] = $docVersion   = $item['ObjVer']['Version'];
                    $data['docType']    = $docType      = $item['ObjVer']['Type'];
                    $data['fileSize']   = $item['Files'][0]['Size'];
                    $data['fileExt']    = $item['Files'][0]['Extension'];

                    $data['docProperties']  = array_reduce($this->getDocumentProperties($docType, $docID, $docVersion), 'array_merge', []);

                    $pemegangHak = $data['docProperties']['namaPemegangHak'] != 'Lain-lain'? $data['docProperties']['namaPemegangHak'] : $data['docProperties']['namaPemilikAwal'];

                    $dataSrtp['doc_id']     = $data['docID'];
                    $dataSrtp['doc_title']  = $data['docTitle'];
                    $dataSrtp['doc_version']= $data['docVersion'];
                    $dataSrtp['doc_type']   = $data['docType'];
                    $dataSrtp['file_size']  = $data['fileSize'];
                    $dataSrtp['file_ext']   = $data['fileExt'];

                    $dataSrtp['index']      = $data['docProperties']['indexNumber'];
                    $dataSrtp['nomor']      = str_pad($data['docProperties']['nomorSertifikat'], 5, '0', STR_PAD_LEFT);
                    $dataSrtp['tgl_buat']   = dateFormatYmd($data['docProperties']['tanggalSertifikat']);
                    $dataSrtp['tgl_akhir']  = dateFormatYmd($data['docProperties']['tanggalValid']);
                    $dataSrtp['jenis']      = $data['docProperties']['jenisSertifikat'];
                    $dataSrtp['tipe']       = $data['docProperties']['tipeSertifikat'];
                    $dataSrtp['luas']       = cleanNominal($data['docProperties']['luas']);
                    $dataSrtp['alamat']     = capitalize($data['docProperties']['alamatObjek']);
                    $dataSrtp['pemegang_hak']= $pemegangHak;
                    $dataSrtp['bus_unit']   = $data['docProperties']['businessUnit'];
                    $dataSrtp['doos']       = $data['docProperties']['doos'];
                    $dataSrtp['rak']        = $data['docProperties']['rak'];
                    $dataSrtp['bast']       = $data['docProperties']['bAST'];
                    $dataSrtp['sup_doc']    = $data['docProperties']['supportingDocument'];
                    $dataSrtp['last_sync']  = Carbon::now();
                    $dataSrtp['last_sync_by']= Auth::user()->id;
                    $dataSrtp['created']    = dateFormatYmdHis($data['docProperties']['created']);
                    $dataSrtp['created_by'] = $data['docProperties']['createdBy'];
                    $dataSrtp['last_modified']= dateFormatYmdHis($data['docProperties']['lastModified']);
                    $dataSrtp['last_modified_by'] = $data['docProperties']['lastModifiedBy'];
                    
                    $cekSrtpExist = Sertipikat::where('doc_title', $row->doc_title);
                    
                    if($cekSrtpExist->count() > 0) {
                        $cekSrtpExist->update($dataSrtp);
                    }
                }
            }
        }

        echo 'success';

    }

    public function search()
    {
        $params['title']    = $this->pageTitle;
        return view('mfiles::sertipikat.search', $params);
    }

    public function searchNow(Request $request)
    {
        $q = $request->q;
        $params['index']    = 1;
        $params['data']     = $this->getDataSertipikatFromMFiles($q);
        return view('mfiles::sertipikat.search-result', $params);
    }

    private function getDataSertipikatFromMFiles($q)
    {
        try {
            ini_set('memory_limit','256M');
            ini_set('max_execution_time','0');
            $dataCollection = [];

            $endPoint   = $this->baseUrl . 'objects.aspx?q=' . $q . '&limit=1000';
            $response   = $this->client->request('GET', $endPoint, [ 'headers' => $this->headers]);
            $result         = json_decode($response->getbody()->getContents(), true);

            if(count($result['Items']) > 0) {
                foreach($result['Items'] as $item) {
                    $data['docTitle']   = $item['Title'];
                    $data['docLastModUtc']  = $item['LastModifiedUtc'];
                    $data['docID']      = $docID        = $item['ObjVer']['ID'];
                    $data['docVersion'] = $docVersion   = $item['ObjVer']['Version'];
                    $data['docType']    = $docType      = $item['ObjVer']['Type'];
                    $data['fileSize']   = $item['Files'][0]['Size'];
                    $data['fileExt']    = $item['Files'][0]['Extension'];
                    $data['class']      = $class = $item['Class'];

                    if($class == 17) {

                        $data['docProperties']  = array_reduce($this->getDocumentProperties($docType, $docID, $docVersion), 'array_merge', []);

                        $pemegangHak = $data['docProperties']['namaPemegangHak'] != 'Lain-lain'? $data['docProperties']['namaPemegangHak'] : $data['docProperties']['namaPemilikAwal'];

                        $dataSrtp['doc_id']     = $data['docID'];
                        $dataSrtp['doc_title']  = $data['docTitle'];
                        $dataSrtp['doc_version']= $data['docVersion'];
                        $dataSrtp['doc_type']   = $data['docType'];
                        $dataSrtp['file_size']  = $data['fileSize'];
                        $dataSrtp['file_ext']   = $data['fileExt'];

                        $dataSrtp['index']      = $data['docProperties']['indexNumber'];
                        $dataSrtp['nomor']      = str_pad($data['docProperties']['nomorSertifikat'], 5, '0', STR_PAD_LEFT);
                        $dataSrtp['tgl_buat']   = dateFormatYmd($data['docProperties']['tanggalSertifikat']);
                        $dataSrtp['tgl_akhir']  = dateFormatYmd($data['docProperties']['tanggalValid']);
                        $dataSrtp['jenis']      = $data['docProperties']['jenisSertifikat'];
                        $dataSrtp['tipe']       = $data['docProperties']['tipeSertifikat'];
                        $dataSrtp['luas']       = cleanNominal($data['docProperties']['luas']);
                        $dataSrtp['alamat']     = capitalize($data['docProperties']['alamatObjek']);
                        $dataSrtp['pemegang_hak']= $pemegangHak;
                        $dataSrtp['bus_unit']   = $data['docProperties']['businessUnit'];
                        $dataSrtp['doos']       = $data['docProperties']['doos'];
                        $dataSrtp['rak']        = $data['docProperties']['rak'];
                        $dataSrtp['bast']       = $data['docProperties']['bAST'];
                        $dataSrtp['sup_doc']    = $data['docProperties']['supportingDocument'];
                        $dataSrtp['created']    = dateFormatYmdHis($data['docProperties']['created']);
                        $dataSrtp['created_by'] = $data['docProperties']['createdBy'];
                        $dataSrtp['last_modified']= dateFormatYmdHis($data['docProperties']['lastModified']);
                        $dataSrtp['last_modified_by'] = $data['docProperties']['lastModifiedBy'];

                        $dataCollection[] = array_merge($dataCollection, $dataSrtp);
                    }
                }

            } 

            return $dataCollection;

        } catch(Exception $e) {
            return [];
        }
    }

    public function syncNow(Request $request)
    {
        try {

            $q = $request->q;
            $data = $this->getDataSertipikatFromMFiles($q);
            
            foreach($data as $row) {

                $dataSrtp['doc_id']     = $row['doc_id'];
                $dataSrtp['doc_title']  = $row['doc_title'];
                $dataSrtp['doc_version']= $row['doc_version'];
                $dataSrtp['doc_type']   = $row['doc_type'];
                $dataSrtp['file_size']  = $row['file_size'];
                $dataSrtp['file_ext']   = $row['file_ext'];

                $dataSrtp['index']      = $row['index'];
                $dataSrtp['nomor']      = $row['nomor'];
                $dataSrtp['tgl_buat']   = $row['tgl_buat'];
                $dataSrtp['tgl_akhir']  = $row['tgl_akhir'];
                $dataSrtp['jenis']      = $row['jenis'];
                $dataSrtp['tipe']       = $row['tipe'];
                $dataSrtp['luas']       = $row['luas'];
                $dataSrtp['alamat']     = $row['alamat'];
                $dataSrtp['pemegang_hak']= $row['pemegang_hak'];
                $dataSrtp['bus_unit']   = $row['bus_unit'];
                $dataSrtp['doos']       = $row['doos'];
                $dataSrtp['rak']        = $row['rak'];
                $dataSrtp['bast']       = $row['bast'];
                $dataSrtp['sup_doc']    = $row['sup_doc'];
                $dataSrtp['last_sync']  = Carbon::now();
                $dataSrtp['last_sync_by']= Auth::user()->id;
                $dataSrtp['created']    = $row['created'];
                $dataSrtp['created_by'] = $row['created_by'];
                $dataSrtp['last_modified']  = $row['last_modified'];
                $dataSrtp['last_modified_by'] = $row['last_modified_by'];
                
                $cekSrtpExist = Sertipikat::where('doc_id', $row['doc_id']);

                if($cekSrtpExist->count() > 0) {

                    $lastModified = dateFormatYmdHis($row['last_modified']);

                    if($cekSrtpExist->where('last_modified', '!=', $lastModified)->count() > 0) {

                        $cekSrtpExist->update($dataSrtp);

                    }

                } else {

                    Sertipikat::create($dataSrtp);

                }
            }

            return response()->json([
                'message' => 'Sertipikat berhasil disimpan'
            ]);

        } catch(Exception $e) {

            return response()->json([
                'message' => trans('message.error_save')
            ]);
        }
    }
}
