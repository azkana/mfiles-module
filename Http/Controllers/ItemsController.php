<?php

namespace Modules\Mfiles\Http\Controllers;

use Excel;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Stream;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Mfiles\Entities\Sertipikat;
use Modules\Mfiles\Exports\DocumentSertipikatExport;
use Modules\Mfiles\Exports\DocumentSertipikatExportToExcel;

class ItemsController extends Controller
{
    protected $pageTitle;
    protected $baseUrl;
    protected $username;
    protected $password;
    protected $vaultID;

    protected $client;
    protected $headers;

    public function __construct()
    {
        $this->pageTitle    = 'M-Files';
        $this->baseUrl      = config('mfiles.base_url');
        $this->username     = config('mfiles.username');
        $this->password     = config('mfiles.password');
        $this->vaultID      = config('mfiles.vault_id');

        $this->client       = new Client();
        $this->headers      = [
            'Accept'        => 'application/json',
            'X-Username'    => $this->username,
            'X-Password'    => $this->password,
            'X-Vault'       => $this->vaultID
        ];
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        $params['pageTitle']    = $this->pageTitle;
        $endPoint       = $this->baseUrl . 'views/items.aspx';
        $response       = $this->client->request('GET', $endPoint, [ 'headers' => $this->headers]);
        $result         = json_decode($response->getbody()->getContents(), true);
        $params['data'] = $result['Items'];
        return view('mfiles::items.index', $params);
    }

    /**
     * Get View By ID
     * @param int $id
     * @return Response
     * @endpoint http://jbmfiles01/REST/views/V{id}/items.aspx 
     */
    public function getViewByID($id) {
        $params['pageTitle']    = $this->pageTitle;
        $endPoint       = $this->baseUrl . 'views/V' . $id . '/items.aspx';
        $response       = $this->client->request('GET', $endPoint, [ 'headers' => $this->headers]);
        $result         = json_decode($response->getbody()->getContents(), true);
        $params['data'] = $result['Items'];
        $params['path'] = $result['Path'];
        return view('mfiles::items.items-by-id', $params);
    }

    /* http://jbmfiles01/REST/views/V{id}/{fID}/items.aspx */
    public function getViewByIDfID($id, $fID) {
        $params['pageTitle']    = $this->pageTitle;
        $endPoint       = $this->baseUrl . 'views/' . $id . '/L' . $fID . '/items?limit=10000';
        $response       = $this->client->request('GET', $endPoint, [ 'headers' => $this->headers, 'stream' => false]);
        $result         = json_decode($response->getbody()->getContents(), true);
        $params['data'] = $result['Items'];
        return view('mfiles::items.items-by-id-fid', $params);
    }

    public function getObjectByID($id) {
        $params['pageTitle']    = $this->pageTitle;
        $endPoint       = $this->baseUrl . 'objects/0/' . $id . '/latest/properties';
        $response       = $this->client->request('GET', $endPoint, [ 'headers' => $this->headers]);
        $result         = json_decode($response->getbody()->getContents(), true);
        $params['data'] = $result;
        return view('mfiles::items.document-by-id', $params);
    }

    public function getDocumentProperty($id = 'V101', $fID = '17') {
        /* Get Data Sertifikat */
        $endPoint   = $this->baseUrl . 'views/' . $id . '/L' . $fID . '/items?limit=10000';
        $response   = $this->client->request('GET', $endPoint, [ 'headers' => $this->headers]);
        $result         = json_decode($response->getbody()->getContents(), true);
        $resultArray = [];
        foreach($result['Items'] as $item) {
            if($item['FolderContentItemType'] == 4) {
                $data['docTitle']   = $item['ObjectVersion']['Title'];
                $data['docLastModUtc']  = $item['ObjectVersion']['LastModifiedUtc'];
                $data['docID']      = $docID        = $item['ObjectVersion']['ObjVer']['ID'];
                $data['docVersion'] = $docVersion   = $item['ObjectVersion']['ObjVer']['Version'];
                $data['docType']    = $docType      = $item['ObjectVersion']['ObjVer']['Type'];
                $data['fileSize']   = $item['ObjectVersion']['Files'][0]['Size'];
                $data['fileExt']    = $item['ObjectVersion']['Files'][0]['Extension'];

                $data['docProperties']  = array_reduce($this->getDocumentProperties($docType, $docID, $docVersion), 'array_merge', []);
                array_push($resultArray, $data);
            }
        }
        $params['pageTitle']    = $this->pageTitle;
        $params['data'] = $resultArray;
        return view('mfiles::items.export', $params);
    }

    public function getPropertyDefDetail($id) {
        $endPoint   = $this->baseUrl . 'structure/properties/' . $id;
        $response       = $this->client->request('GET', $endPoint, [ 'headers' => $this->headers]);
        $result         = json_decode($response->getbody()->getContents(), true);
        return $result['Name'];
    }

    private function getDocumentProperties($type, $id, $version) {
        $endPoint   = $this->baseUrl . 'objects/properties;' . $type . '/' . $id . '/' . $version;
        $response   = $this->client->request('GET', $endPoint, [ 'headers' => $this->headers]);
        $result     = json_decode($response->getbody()->getContents(), true);
        $resultArray= [];
        foreach($result[0] as $item) {
            $arrayData = array(
                camel_case($this->getPropertyDefDetail($item['PropertyDef'])) => $item['Value']['DisplayValue']
            );
            array_push($resultArray, $arrayData);
        }
        return array_merge($resultArray);
    }

    /**
     * Export from M-Files to Excel directly
     */
    public function exportExcel($id = 'V101', $fID = '17')
    {
        ini_set('memory_limit','256M');
        ini_set('max_execution_time','0');
        $endPoint   = $this->baseUrl . 'views/' . $id . '/L' . $fID . '/items?limit=10000';
        $response   = $this->client->request('GET', $endPoint, [ 'headers' => $this->headers]);
        $result         = json_decode($response->getbody()->getContents(), true);
        $resultArray = [];
        foreach($result['Items'] as $item) {
            if($item['FolderContentItemType'] == 4) {
                $data['docTitle']   = $item['ObjectVersion']['Title'];
                $data['docLastModUtc']  = $item['ObjectVersion']['LastModifiedUtc'];
                $data['docID']      = $docID        = $item['ObjectVersion']['ObjVer']['ID'];
                $data['docVersion'] = $docVersion   = $item['ObjectVersion']['ObjVer']['Version'];
                $data['docType']    = $docType      = $item['ObjectVersion']['ObjVer']['Type'];
                $data['fileSize']   = $item['ObjectVersion']['Files'][0]['Size'];
                $data['fileExt']    = $item['ObjectVersion']['Files'][0]['Extension'];

                $data['docProperties']  = array_reduce($this->getDocumentProperties($docType, $docID, $docVersion), 'array_merge', []);
                array_push($resultArray, $data);
            }
        }
        return Excel::download(new DocumentSertipikatExport($resultArray), 'sertipikat_'.date('Y-m-d').'.xlsx');
    }

    /**
     * Export from M-Files to Excel with sync to local database
     */
    public function syncByIDfID($id = 'V101', $fID = '17')
    {
        ini_set('memory_limit','256M');
        ini_set('max_execution_time','0');
        $endPoint   = $this->baseUrl . 'views/' . $id . '/L' . $fID . '/items?limit=1000';
        $response   = $this->client->request('GET', $endPoint, [ 'headers' => $this->headers]);
        $result         = json_decode($response->getbody()->getContents(), true);
        
        foreach($result['Items'] as $item) {
            if($item['FolderContentItemType'] == 4) {
                $data['docTitle']   = $item['ObjectVersion']['Title'];
                $data['docLastModUtc']  = $item['ObjectVersion']['LastModifiedUtc'];
                $data['docID']      = $docID        = $item['ObjectVersion']['ObjVer']['ID'];
                $data['docVersion'] = $docVersion   = $item['ObjectVersion']['ObjVer']['Version'];
                $data['docType']    = $docType      = $item['ObjectVersion']['ObjVer']['Type'];
                $data['fileSize']   = $item['ObjectVersion']['Files'][0]['Size'];
                $data['fileExt']    = $item['ObjectVersion']['Files'][0]['Extension'];

                $data['docProperties']  = array_reduce($this->getDocumentProperties($docType, $docID, $docVersion), 'array_merge', []);

                $pemegangHak = $data['docProperties']['namaPemegangHak'] != 'Lain-lain'? $data['docProperties']['namaPemegangHak'] : $data['docProperties']['namaPemilikAwal'];

                $dataSrtp['doc_id']     = $data['docID'];
                $dataSrtp['doc_title']  = $data['docTitle'];
                $dataSrtp['doc_version']= $data['docVersion'];
                $dataSrtp['doc_type']   = $data['docType'];
                $dataSrtp['file_size']  = $data['fileSize'];
                $dataSrtp['file_ext']   = $data['fileExt'];

                $dataSrtp['index']      = $data['docProperties']['indexNumber'];
                $dataSrtp['nomor']      = str_pad($data['docProperties']['nomorSertifikat'], 5, '0', STR_PAD_LEFT);
                $dataSrtp['tgl_buat']   = dateFormatYmd($data['docProperties']['tanggalSertifikat']);
                $dataSrtp['tgl_akhir']  = dateFormatYmd($data['docProperties']['tanggalValid']);
                $dataSrtp['jenis']      = $data['docProperties']['jenisSertifikat'];
                $dataSrtp['tipe']       = $data['docProperties']['tipeSertifikat'];
                $dataSrtp['luas']       = cleanNominal($data['docProperties']['luas']);
                $dataSrtp['alamat']     = $data['docProperties']['alamatObjek'];
                $dataSrtp['pemegang_hak']= $pemegangHak;
                $dataSrtp['bus_unit']   = $data['docProperties']['businessUnit'];
                $dataSrtp['doos']       = $data['docProperties']['doos'];
                $dataSrtp['rak']        = $data['docProperties']['rak'];
                $dataSrtp['bast']       = $data['docProperties']['bAST'];
                $dataSrtp['sup_doc']    = $data['docProperties']['supportingDocument'];
                $dataSrtp['last_sync']  = Carbon::now();
                $dataSrtp['last_sync_by']= Auth::user()->id;
                $dataSrtp['created']    = dateFormatYmdHis($data['docProperties']['created']);
                $dataSrtp['created_by'] = $data['docProperties']['createdBy'];
                $dataSrtp['last_modified']= dateFormatYmdHis($data['docProperties']['lastModified']);
                $dataSrtp['last_modified_by'] = $data['docProperties']['lastModifiedBy'];

                $cekSrtpExist = Sertipikat::where('doc_id', $data['docID']);
                if($cekSrtpExist->count() > 0) {
                    $lastModified = dateFormatYmdHis($data['docProperties']['lastModified']);
                    if($cekSrtpExist->where('last_modified', '!=', $lastModified)->count() > 0) {
                        $cekSrtpExist->update($dataSrtp);
                    }
                } else {
                    Sertipikat::create($dataSrtp);
                }
            }
        }
        
        return $this->exportToExcel();
    }

    private function exportToExcel()
    {
        $data = Sertipikat::orderBy('index')->get();

        return Excel::download(new DocumentSertipikatExportToExcel($data), 'sertipikat_'.date('Y-m-d').'.xlsx');
    }
}
