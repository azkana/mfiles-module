<?php

return [
    'name' => 'Mfiles',

    'base_url' => env('MFILES_BASE_URL'),

    'username'  => env('MFILES_USERNAME'),

    'password'  => env('MFILES_PASSWORD'),

    'vault_id'  => '{' . env('MFILES_VAULT_ID') . '}',
];
