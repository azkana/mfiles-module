<?php

namespace Modules\Mfiles\Entities;

use App\Models\User;
use App\Traits\Uuid as TraitsUuid;
use Illuminate\Database\Eloquent\Model;

class Warkah extends Model
{
    use TraitsUuid;

    protected $table = 'mfl_warkah';

    public $incrementing    = false;

    protected $dates = [
        'sph_dt',
        'created',
        'last_modified',
    ];

    protected $fillable = [
        'index',
        'kode',
        'nomor',
        'name',
        'jenis',
        'luas',
        'desa',
        'kecamatan',
        'kabupaten',
        'sph_no',
        'sph_nm',
        'sph_dt',
        'sph_ls',
        'ktp',
        'kk',
        'sk_sengketa',
        'sk_waris',
        'k_waris',
        'k_jual',
        'pbb',
        'created',
        'created_by',
        'last_modified',
        'last_modified_by',
        'last_sync',
        'last_sync_by',
        'doc_id',
        'doc_title',
        'doc_version',
        'doc_type',
        'file_size',
        'file_ext',
    ];

    public function lastSyncBy()
    {
        return $this->belongsTo(User::class, 'last_sync_by')->withDefault([
            'name' => null
        ]);
    }
}
