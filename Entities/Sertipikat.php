<?php

namespace Modules\Mfiles\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid as TraitsUuid;
use App\Models\User;

class Sertipikat extends Model
{
    use TraitsUuid;

    protected $table = 'mfl_sertipikat';

    public $incrementing    = false;

    protected $dates = [
        'tgl_buat',
        'tgl_akhir',
        'created',
        'last_modified',
    ];

    protected $fillable = [
        'index',
        'kode',
        'nomor',
        'tgl_buat',
        'tgl_akhir',
        'jenis',
        'tipe',
        'luas',
        'alamat',
        'pemegang_hak',
        'bus_unit',
        'doos',
        'rak',
        'bast',
        'sup_doc',
        'last_sync',
        'last_sync_by',
        'doc_id',
        'doc_title',
        'doc_version',
        'doc_type',
        'file_size',
        'file_ext',
        'created',
        'created_by',
        'last_modified',
        'last_modified_by'
    ];

    public function lastSyncBy()
    {
        return $this->belongsTo(User::class, 'last_sync_by')->withDefault([
            'name' => null
        ]);
    }
}
