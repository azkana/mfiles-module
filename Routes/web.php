<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
    Route::prefix('manage')->group(function() {
        Route::prefix('m-files')->group(function() {
            
            Route::name('item.')->group(function() {
                Route::get('items/exports/{id}/{fID}', 'ItemsController@exportExcel')->name('byidfid.export');
                Route::get('items', 'ItemsController@index')->name('index');
                Route::get('items/{id}', 'ItemsController@getViewByID')->name('id');
                Route::get('items/object/{id}', 'ItemsController@getObjectByID')->name('object.byid');
                Route::get('items/prop/{id}', 'ItemsController@getPropertyDefDetail')->name('prop.id');
                Route::get('items/{id}/{fID}', 'ItemsController@getViewByIDfID')->name('byidfid');
                
                Route::get('items/sync/{id}/{fID}', 'ItemsController@syncByIDfID')->name('sync.byidfid');
            });

            Route::name('sertipikat.')->group(function() {
                Route::get('sertipikat', 'SertipikatController@index')->name('index');
                Route::get('sertipikat/data', 'SertipikatController@indexData')->name('index.data');
                Route::get('sertipikat/show', 'SertipikatController@show')->name('show');
                Route::get('sertipikat/sync', 'SertipikatController@sync')->name('sync');
                Route::get('sertipikat/sync-data', 'SertipikatController@syncDataNull')->name('sync.data');
                Route::get('sertipikat/export', 'SertipikatController@exportToExcel')->name('export');

                Route::get('sertipikat/search', 'SertipikatController@search')->name('search');
                Route::get('sertipikat/search/now', 'SertipikatController@searchNow')->name('search.now');
                Route::get('sertipikat/sync/now', 'SertipikatController@syncNow')->name('sync.now');
            });

            Route::name('warkah.')->group(function() {
                Route::get('warkah', 'WarkahController@index')->name('index');
                Route::get('warkah/data', 'WarkahController@indexData')->name('index.data');
                Route::get('warkah/show', 'WarkahController@show')->name('show');
                Route::get('warkah/sync', 'WarkahController@sync')->name('sync');
                Route::get('warkah/export', 'WarkahController@exportToExcel')->name('export');
            });

        });
    });
});
