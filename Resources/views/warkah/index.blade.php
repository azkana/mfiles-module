@extends('mfiles::layouts.master')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                        @can('mfiles-warkah-sync')
                            <a class="btn btn-sm btn-info" id="btn-sync">
                                <i class="fa fa-refresh"></i> Sync
                            </a>
                        @endcan
                        @can('mfiles-warkah-export')
                            <a class="btn btn-sm btn-success" href="{!! route('warkah.export') !!}">
                                <i class="fa fa-file-excel-o"></i> Export to Excel
                            </a>
                        @endcan
                    </div>
                </div>
                <div class="box-body" style="min-height: 500px">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="grid-warkah" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="vertical-align: middle; width: 2%">#</th>
                                        <th class="text-center" style="vertical-align: middle; width: 5%">Nomor</th>
                                        <th class="text-center" style="vertical-align: middle;">Nama</th>
                                        <th class="text-center" style="vertical-align: middle; width: 10%">Jenis</th>
                                        <th class="text-center" style="vertical-align: middle; width: 5%">Luas (m<sup>2</sup>)</th>
                                        <th class="text-center" style="vertical-align: middle; width: 10%">Desa</th>
                                        <th class="text-center" style="vertical-align: middle; width: 10%">Kabupaten</th>
                                        <th class="text-center" style="vertical-align: middle; width: 100px">Modified</th>
                                        <th class="text-center" style="vertical-align: middle; width: 5%">
                                            <i class="fa fa-navicon"></i>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="form-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="detail">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div id="content"></div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            var btnSync = $('#btn-sync');
            @can('mfiles-warkah-sync')
            btnSync.on('click', function(e) {
                btnSync.attr('disabled', true);
                $.ajax({
                    url: "{!! route('warkah.sync') !!}",
                    type: "GET",
                    success: function(data) {
                        toastr.success(data.message);
                        $('#grid-warkah').DataTable().ajax.reload(null, false);
                        btnSync.attr('disabled', false);
                    },
                    error: function(error) {
                        toastr.error(error.message);
                        btnSync.attr('disabled', false);
                    }
                });
            });
            @endcan

            var table = $('#grid-warkah').DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                PaginationType: "two_button",
                paginate:true,
                deferRender: true,
                keys: false,
                dom: '<"row"<"col-md-4"l><"col-md-4 text-center"B><"col-md-4"f>>' + 'rtip',
                buttons: [
                    {
                        text: '<i class="fa fa-refresh"></i> Refresh',
                        className: "btn-sm",
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                ],
                ajax: { 
                    url: "{!! route('warkah.index.data') !!}",
                    pages: 10,
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', className: "small", orderable: false, searchable: false},
                    {data: 'nomor', name: 'nomor', className: "small"},
                    {data: 'name', name: 'name', className: "small"},
                    {data: 'jenis', name: 'jenis', className: "small"},
                    {data: 'luas', name: 'luas', className: "small text-right"},
                    {data: 'desa', name: 'desa', className: "small"},
                    {data: 'kabupaten', name: 'kabupaten', className: "small"},
                    {data: 'last_modified', name: 'last_modified', className: "small"},
                    {data: 'action', name: 'action', className: "small text-center", orderable: false, searchable: false},
                ],
                columnDefs: [
                    {targets: 4, render: $.fn.dataTable.render.number( '.', ',', 0, '' )},
                    {targets: 7, render: $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD-MM-YYYY HH:mm' )},
                ],
                order: [
                    [7, 'desc']
                ],
                language: {
                    processing: "<p class='text-center'>Loading...</p>"
                },
            });
        });

        function showDetail(id) {
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{!! route('warkah.show') !!}",
                data: {id: id},
                success: function(data) {
                    $('#form-modal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#content').html(data);
                }
            });
        }

    </script>
@endsection