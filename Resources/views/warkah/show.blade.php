<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="detail">Detail {!! $title !!}</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <table class="table table-condensed table-striped">
                <tbody>
                    <tr>
                        <th style="width: 40%">Nomor</th>
                        <td>{!! $data->nomor !!}</td>
                    </tr>
                    <tr>
                        <th>Nama</th>
                        <td>{!! $data->name !!}</td>
                    </tr>
                    <tr>
                        <th>Jenis Dokumen</th>
                        <td>{!! $data->jenis !!}</td>
                    </tr>
                    <tr>
                        <th>Luas</th>
                        <td>{!! numberFormat($data->luas) !!} m<sup>2</sup></td>
                    </tr>
                    <tr>
                        <th>Desa</th>
                        <td>{!! $data->desa !!}</td>
                    </tr>
                    <tr>
                        <th>Kecamatan</th>
                        <td>{!! $data->kecamatan !!}</td>
                    </tr>
                    <tr>
                        <th>Kabupaten</th>
                        <td>{!! $data->kabupaten !!}</td>
                    </tr>
                    <tr>
                        <th>Nomor SPH</th>
                        <td>{!! $data->sph_no !!}</td>
                    </tr>
                    <tr>
                        <th>Nama SPH</th>
                        <td>{!! $data->sph_nm !!}</td>
                    </tr>
                    <tr>
                        <th>Tgl. SPH</th>
                        <td>{!! dateFormatDmy($data->sph_dt) !!}</td>
                    </tr>
                    <tr>
                        <th>Luas SPH</th>
                        <td>{!! numberFormat($data->sph_ls) !!} m<sup>2</sup></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
            <table class="table table-condensed table-striped">
                <tbody>
                    <tr>
                        <th style="width: 60%">KTP</th>
                        <td>{!! __getStateDetail($data->ktp) !!}</td>
                    </tr>
                    <tr>
                        <th>KK</th>
                        <td>{!! __getStateDetail($data->kk) !!}</td>
                    </tr>
                    <tr>
                        <th>Surat Keterangan Sengketa</th>
                        <td>{!! __getStateDetail($data->sk_sengketa) !!}</td>
                    </tr>
                    <tr>
                        <th>Surat Keterangan Waris</th>
                        <td>{!! __getStateDetail($data->sk_waris) !!}</td>
                    </tr>
                    <tr>
                        <th>Kuasa Waris</th>
                        <td>{!! __getStateDetail($data->k_waris) !!}</td>
                    </tr>
                    <tr>
                        <th>Kuasa Jual</th>
                        <td>{!! __getStateDetail($data->k_jual) !!}</td>
                    </tr>
                    <tr>
                        <th>PBB</th>
                        <td>{!! __getStateDetail($data->pbb) !!}</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Created</th>
                        <td>{!! dateFormatDmyHi($data->created) !!} WIB</td>
                    </tr>
                    <tr>
                        <th>Created By</th>
                        <td>{!! capitalize($data->created_by) !!}</td>
                    </tr>
                    <tr>
                        <th>Last Modified</th>
                        <td>{!! dateFormatDmyHi($data->last_modified) !!} WIB</td>
                    </tr>
                    <tr>
                        <th>Last Modified By</th>
                        <td>{!! capitalize($data->last_modified_by) !!}</td>
                    </tr>
                    <tr>
                        <th>Last Sync</th>
                        <td>{!! dateFormatDmyHi($data->last_sync) !!} WIB</td>
                    </tr>
                    <tr>
                        <th>Last Sync By</th>
                        <td>{!! capitalize($data->lastSyncBy->name) !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>