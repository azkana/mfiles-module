@extends('mfiles::layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"></h3>
                <div class="box-tools pull-right">
                    <a class="btn btn-sm btn-success" href="{!! route('item.sync.byidfid', [Request()->segment(4), Request()->segment(5)]) !!}">
                        <i class="fa fa-file-excel-o"></i> Excel
                    </a>
                    {{-- <a class="btn btn-sm btn-danger" href="{!! URL::previous() !!}">
                        <i class="fa fa-times"></i>
                    </a> --}}
                </div>
            </div>
            <div class="box-body" style="min-height: 520px">
                <table id="grid-items" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width: 5%">No.</th>
                            <th>Name</th>
                            <th style="width: 80px">Size</th>
                            <th style="width: 100px">Date Modified</th>
                        </tr>
                    </thead>
                    <tbody class="small">
                        @foreach ($data as $items)
                        <tr>
                            <td></td>
                            <td>
                                @if($items['FolderContentItemType'] == 4)
                                    <a href="{!! route('item.object.byid', $items['ObjectVersion']['DisplayID']) !!}" title="View">
                                        {!! $items['ObjectVersion']['Title'] !!}
                                    </a>
                                {{-- @elseif($items['FolderContentItemType'] == 2)
                                    <a href="{!! route('view.byidfid', [$path, $items['PropertyFolder']['SerializedValue']]) !!}" title="Virtual Folder">
                                        {!! $items['PropertyFolder']['DisplayValue'] !!}
                                    </a> --}}
                                @endif
                            </td>
                            <td class="text-right">
                                {!! formatSizeUnits($items['ObjectVersion']['Files'][0]['Size']) !!}
                            </td>
                            <td class="text-right">
                                @php
                                    setLocale(LC_TIME, 'id');
                                    $utc = Carbon::parse($items['ObjectVersion']['LastModified'])->setTimezone('UTC');
                                    $idTz= Carbon::createFromFormat('Y-m-d H:i:s', $utc, 'UTC');
                                @endphp
                                {!! $idTz->setTimezone('Asia/Jakarta')->format('d-M-Y H:i:s') !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            var t = $("#grid-items").DataTable({
                "columnDefs": [ 
                    {
                        "targets": 0,
                        "searchable": false,
                        "orderable": false,
                        "className": "dt-body-center",
                    },
                    {"targets": 2, "orderable": false},
                ],
                "order": [[3, 'asc']],
            });

            t.on('order.dt search.dt', function() {
                t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                    cell.innerHTML = i+1;
                });
            }).draw();
        });
    </script>
@endsection