@extends('mfiles::layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"></h3>
                <div class="box-tools pull-right">
                    <a class="btn btn-sm btn-danger" href="{!! route('item.index') !!}">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="box-body" style="min-height: 520px">
                <table id="grid-items" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width: 5%">No.</th>
                            <th>Name</th>
                            <th>Type</th>
                        </tr>
                    </thead>
                    <tbody class="small">
                        @foreach ($data as $items)
                        <tr>
                            <td></td>
                            <td>
                                @if($items['FolderContentItemType'] == 1)
                                <a href="{!! route('item.id', $items['View']['ID']) !!}">
                                    {!! $items['View']['Name'] !!}
                                </a>
                                @elseif($items['FolderContentItemType'] == 2)
                                <a href="{!! route('item.byidfid', [ substr($path,1), $items['PropertyFolder']['SerializedValue']]) !!}" title="Virtual Folder">
                                    {!! $items['PropertyFolder']['DisplayValue'] !!}
                                </a>
                                @endif
                            </td>
                            <td>
                                @if($items['FolderContentItemType'] == 1)
                                View
                                @elseif($items['FolderContentItemType'] == 2)
                                Virtual Folder
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            var t = $("#grid-items").DataTable({
                "columnDefs": [ 
                    {
                        "targets": 0,
                        "searchable": false,
                        "orderable": false,
                        "className": "dt-body-center",
                    },
                    {"targets": 2, "orderable": false},
                ],
                "order": [[1, 'asc']],
            });

            t.on('order.dt search.dt', function() {
                t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                    cell.innerHTML = i+1;
                });
            }).draw();
        });
    </script>
@endsection