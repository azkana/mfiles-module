@extends('mfiles::layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Detail <b>{!! __getDetailPropertyDef($data, 1101, 1) !!}</b></h3>
                <div class="box-tools pull-right">
                    <a class="btn btn-sm btn-danger" href="{!! URL::previous() !!}">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="box-body" style="min-height: 520px">
                <table id="grid-items" class="table table-bordered table-striped">
                    <tr>
                        <th width="40%">Class</th>
                        <td>{!! __getDetailPropertyDef($data, 100, 9) !!}</td>
                    </tr>
                    <tr>
                        <th width="">Nama</th>
                        <td>{!! __getDetailPropertyDef($data, 1101, 1) !!}</td>
                    </tr>
                    <tr>
                        <th width="">Nomor Sertifikat</th>
                        <td>{!! __getDetailPropertyDef($data, 1102, 1) !!}</td>
                    </tr>
                    <tr>
                        <th width="">Tgl. Sertifikat</th>
                        <td>{!! __getDetailPropertyDef($data, 1103, 5) !!}</td>
                    </tr>
                    <tr>
                        <th width="">Tipe Sertifikat</th>
                        <td>{!! __getDetailPropertyDef($data, 1108, 9) !!}</td>
                    </tr>
                    <tr>
                        <th width="">Jenis Sertifikat</th>
                        <td>{!! __getDetailPropertyDef($data, 1105, 9) !!}</td>
                    </tr>
                    <tr>
                        <th width="">BAST</th>
                        <td>{!! __getDetailPropertyDef($data, 1113, 1) !!}</td>
                    </tr>
                    <tr>
                        <th width="">Alamat Objek</th>
                        <td>{!! __getDetailPropertyDef($data, 1068, 13) !!}</td>
                    </tr>
                    <tr>
                        <th width="">Nama Pemegang Hak</th>
                        <td>{!! __getDetailPropertyDef($data, 1126, 9) !!}</td>
                    </tr>
                    <tr>
                        <th width="">Business Unit</th>
                        <td>{!! __getDetailPropertyDef($data, 1152, 9) !!}</td>
                    </tr>
                    <tr>
                        <th width="">Tgl. Valid</th>
                        <td>{!! __getDetailPropertyDef($data, 1106, 5) !!}</td>
                    </tr>
                    <tr>
                        <th width="">Luas</th>
                        <td>{!! __getDetailPropertyDef($data, 1097, 1) !!} <small>m<sup>2</sup></small></td>
                    </tr>
                    <tr>
                        <th width="">Doos</th>
                        <td>{!! __getDetailPropertyDef($data, 1111, 1) !!}</td>
                    </tr>
                    <tr>
                        <th width="">Rak</th>
                        <td>{!! __getDetailPropertyDef($data, 1124, 9) !!}</td>
                    </tr>
                    <tr>
                        <th width="">Supporting Document</th>
                        <td>{!! __getDetailPropertyDef($data, 1051, 10) !!}</td>
                    </tr>
                    <tr>
                        <th width="">Created By</th>
                        <td>
                            {!! __getDetailPropertyDef($data, 25, 9) !!}
                            <small>({!! __getDetailPropertyDef($data, 20, 7) !!})</small>
                        </td>
                    </tr>
                    <tr>
                        <th width="">Last Modified By</th>
                        <td>
                            {!! __getDetailPropertyDef($data, 23, 9) !!}
                            <small>({!! __getDetailPropertyDef($data, 21, 7) !!})</small>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection