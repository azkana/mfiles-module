<table>
    <thead>
        <tr>
            <th rowspan="2">Index</th>
            <th rowspan="2">Nama</th>
            <th colspan="6">Sertipikat</th>
            <th rowspan="2">Alamat</th>
            <th rowspan="2">Pemegang Hak</th>
            <th rowspan="2">Busines Unit</th>
            <th rowspan="2">Doos</th>
            <th rowspan="2">Rak</th>
            <th rowspan="2">BAST</th>
            <th rowspan="2">Supporting Document</th>
        </tr>
        <tr>
            <th>Nomor</th>
            <th>Tanggal Sertifikat</th>
            <th>Tanggal Valid</th>
            <th>Jenis</th>
            <th>Tipe</th>
            <th>Luas</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $items)
        <tr>
            <td>{!! $items->index !!}</td>
            <td>{!! $items->doc_title !!}</td>
            <td>{!! $items->nomor !!}</td>
            <td>{!! dateFormatDmy($items->tgl_buat) !!}</td>
            <td>{!! dateFormatDmy($items->tgl_akhir) !!}</td>
            <td>{!! $items->jenis !!}</td>
            <td>{!! $items->tipe !!}</td>
            <td>{!! numberFormat($items->luas) !!}</td>
            <td>{!! $items->alamat !!}</td>
            <td>{!! $items->pemegang_hak !!}</td>
            <td>{!! $items->bus_unit !!}</td>
            <td>{!! $items->doos !!}</td>
            <td>{!! $items->rak !!}</td>
            <td>{!! $items->bast !!}</td>
            <td>{!! $items->sup_doc !!}</td>
        </tr>
        @endforeach
    </tbody>
</table>