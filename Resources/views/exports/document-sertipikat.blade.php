<table>
    <thead>
        <tr>
            <th rowspan="2">Nama</th>
            <th colspan="4">Sertipikat</th>
            <th rowspan="2">Alamat</th>
            <th rowspan="2">Pemegang Hak</th>
            <th rowspan="2">Busines Unit</th>
            <th rowspan="2">Tgl. Valid</th>
            <th rowspan="2">Doos</th>
            <th rowspan="2">Rak</th>
            <th rowspan="2">Luas</th>
            <th rowspan="2">BAST</th>
            <th rowspan="2">Supporting Document</th>
        </tr>
        <tr>
            <th>Nomor</th>
            <th>Tanggal</th>
            <th>Jenis</th>
            <th>Tipe</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $items)
        <tr>
            <td>{!! isset($items['docProperties']['nameOrTitle']) ? $items['docProperties']['nameOrTitle'] : null !!}</td>
            <td>{!! isset($items['docProperties']['nomorSertifikat']) ? $items['docProperties']['nomorSertifikat'] : null !!}</td>
            <td>{!! isset($items['docProperties']['tanggalSertifikat']) ? dateFormatDmy($items['docProperties']['tanggalSertifikat']) : null !!}</td>
            <td>{!! isset($items['docProperties']['jenisSertifikat']) ? $items['docProperties']['jenisSertifikat'] : null !!}</td>
            <td>{!! isset($items['docProperties']['tipeSertifikat']) ? $items['docProperties']['tipeSertifikat'] : null !!}</td>
            <td>{!! isset($items['docProperties']['alamatObjek']) ? $items['docProperties']['alamatObjek'] : null !!}</td>
            <td>{!! isset($items['docProperties']['namaPemegangHak']) ? $items['docProperties']['namaPemegangHak'] : null !!}</td>
            <td>{!! isset($items['docProperties']['businessUnit']) ? $items['docProperties']['businessUnit'] : null !!}</td>
            <td>{!! isset($items['docProperties']['tanggalValid']) ? dateFormatDmy($items['docProperties']['tanggalValid']) : null !!}</td>
            <td>{!! isset($items['docProperties']['doos']) ? $items['docProperties']['doos'] : null !!}</td>
            <td>{!! isset($items['docProperties']['rak']) ? $items['docProperties']['rak'] : null !!}</td>
            <td>{!! isset($items['docProperties']['luas']) ? cleanNominal($items['docProperties']['luas']) : null !!}</td>
            <td>{!! isset($items['docProperties']['bAST']) ? $items['docProperties']['bAST'] : null !!}</td>
            <td>{!! isset($items['docProperties']['supportingDocument']) ? $items['docProperties']['supportingDocument'] : null !!}</td>
        </tr>
        @endforeach
    </tbody>
</table>