<table>
    <thead>
        <tr>
            <th rowspan="2">Nomor</th>
            <th rowspan="2">Nama</th>
            <th rowspan="2">Jenis</th>
            <th rowspan="2">Luas</th>
            <th rowspan="2">Desa</th>
            <th rowspan="2">Kecamatan</th>
            <th rowspan="2">Kabupaten</th>
            <th colspan="4">SPH</th>
            <th colspan="7">Data Pendukung</th>
        </tr>
        <tr>
            <th>Nomor</th>
            <th>Nama</th>
            <th>Tanggal</th>
            <th>Luas</th>
            <th>KTP</th>
            <th>KK</th>
            <th>Ket. Sengketa</th>
            <th>Ket. Waris</th>
            <th>Kuasa Waris</th>
            <th>Kuasa Jual</th>
            <th>PBB</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $items)
        <tr>
            <td>{!! $items->nomor !!}</td>
            <td>{!! $items->name !!}</td>
            <td>{!! $items->jenis !!}</td>
            <td>{!! $items->luas !!}</td>
            <td>{!! $items->desa !!}</td>
            <td>{!! $items->kecamatan !!}</td>
            <td>{!! $items->kabupaten !!}</td>
            <td>{!! $items->sph_no !!}</td>
            <td>{!! $items->sph_nm !!}</td>
            <td>{!! dateFormatDmy($items->sph_dt) !!}</td>
            <td>{!! $items->sph_ls !!}</td>
            <td>{!! __getStateDetail($items->ktp) !!}</td>
            <td>{!! __getStateDetail($items->kk) !!}</td>
            <td>{!! __getStateDetail($items->sk_sengketa) !!}</td>
            <td>{!! __getStateDetail($items->sk_waris) !!}</td>
            <td>{!! __getStateDetail($items->k_waris) !!}</td>
            <td>{!! __getStateDetail($items->k_jual) !!}</td>
            <td>{!! __getStateDetail($items->pbb) !!}</td>
        </tr>
        @endforeach
    </tbody>
</table>