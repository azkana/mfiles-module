@can('mfiles')
<li class="treeview @if($adminActiveMenu == 'm-files') active @endif">
    <a href="#">
        <i class="fa fa-file-text-o"></i>
        <span>M-Files</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        @can('mfiles-sertifikat-read')
            <li class="@if($adminActiveSubMenu == 'sertipikat') active @endif"><a href="{!! route('sertipikat.index') !!}"><i class="fa fa-circle-o"></i> Sertifikat</a></li>
        @endcan
        @can('mfiles-warkah-read')
            <li class="@if($adminActiveSubMenu == 'warkah') active @endif"><a href="{!! route('warkah.index') !!}"><i class="fa fa-circle-o"></i> Warkah</a></li>
        @endcan
    </ul>
</li>
@endcan