<hr>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table id="grid-sertipikat-search" class="table table-bordered table-striped small" style="margin-top: 20px">
                <thead>
                    <tr>
                        <th class="text-center" style="vertical-align: middle; width: 2%">#</th>
                        <th class="text-center" style="vertical-align: middle; width: 5%">Jenis</th>
                        <th class="text-center" style="vertical-align: middle; width: 5%">Tipe</th>
                        <th class="text-center" style="vertical-align: middle; width: 5%">Nomor</th>
                        <th class="text-center" style="vertical-align: middle;">Pemegang<br>Hak</th>
                        <th class="text-center" style="vertical-align: middle; width: 5%">Luas<br>(m<sup>2</sup>)</th>
                        <th class="text-center" style="vertical-align: middle; width: 10%">Alamat</th>
                        <th class="text-center" style="vertical-align: middle; width: 7%">Tgl.<br>Sertipikat</th>
                        <th class="text-center" style="vertical-align: middle; width: 9%">Tgl.<br>Valid</th>
                        <th class="text-center" style="vertical-align: middle; ">Business<br>Unit</th>
                        <th class="text-center" style="vertical-align: middle; ">Dokumen<br>Title</th>
                        <th class="text-center" style="vertical-align: middle; width: 80px">Modified</th>
                        {{-- <th class="text-center" style="vertical-align: middle; width: 5%">
                            <i class="fa fa-navicon"></i>
                        </th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $row)
                        <tr class="small">
                            <td>{!! $index++ !!}</td>
                            <td>{!! $row['jenis'] !!}</td>
                            <td>{!! $row['tipe'] !!}</td>
                            <td>{!! $row['nomor'] !!}</td>
                            <td>{!! $row['pemegang_hak'] !!}</td>
                            <td>{!! numberFormat($row['luas']) !!}</td>
                            <td>{!! $row['alamat'] !!}</td>
                            <td>{!! $row['tgl_buat'] !!}</td>
                            <td>{!! $row['tgl_akhir'] !!}</td>
                            <td>{!! $row['bus_unit'] !!}</td>
                            <td>{!! $row['doc_title'] !!}</td>
                            <td>{!! $row['last_modified'] !!}</td>
                            {{-- <td></td> --}}
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#grid-sertipikat-search').DataTable({
            ordering: false
        });
    });
</script>