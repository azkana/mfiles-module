@extends('mfiles::layouts.master')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                        @can('mfiles-sertipikat-search')
                            <a class="btn btn-sm btn-default" onclick="showSearch(); return false;">
                                <i class="fa fa-search"></i> Search
                            </a>
                        @endcan
                        @can('mfiles-sertipikat-sync')
                            <a class="btn btn-sm btn-info" id="btn-sync">
                                <i class="fa fa-refresh"></i> Sync
                            </a>
                        @endcan
                        @can('mfiles-sertipikat-export')
                            <a class="btn btn-sm btn-success" href="{!! route('sertipikat.export') !!}">
                                <i class="fa fa-file-excel-o"></i> Export to Excel
                            </a>
                        @endcan
                    </div>
                </div>
                <div class="box-body" style="min-height: 500px">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="grid-sertipikat" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="vertical-align: middle; width: 2%">#</th>
                                        <th class="text-center" style="vertical-align: middle; width: 5%">Jenis</th>
                                        <th class="text-center" style="vertical-align: middle; width: 5%">Tipe</th>
                                        <th class="text-center" style="vertical-align: middle; width: 5%">Nomor</th>
                                        <th class="text-center" style="vertical-align: middle;">Pemegang<br>Hak</th>
                                        <th class="text-center" style="vertical-align: middle; width: 5%">Luas<br>(m<sup>2</sup>)</th>
                                        <th class="text-center" style="vertical-align: middle; width: 10%">Alamat</th>
                                        <th class="text-center" style="vertical-align: middle; width: 7%">Tgl.<br>Sertipikat</th>
                                        <th class="text-center" style="vertical-align: middle; width: 7%">Tgl.<br>Valid</th>
                                        <th class="text-center" style="vertical-align: middle; ">Business<br>Unit</th>
                                        <th class="text-center" style="vertical-align: middle; width: 80px">Modified</th>
                                        <th class="text-center" style="vertical-align: middle; width: 5%">
                                            <i class="fa fa-navicon"></i>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="form-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="detail">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div id="content"></div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            @can('mfiles-sertipikat-sync')
            $('#btn-sync').on('click', function(e) {
                $.ajax({
                    url: "{!! route('sertipikat.sync') !!}",
                    type: "GET",
                    success: function(data) {
                        toastr.success(data.message);
                        $('#grid-sertipikat').DataTable().ajax.reload(null, false);
                    },
                    error: function(error) {
                        toastr.error(error.message);
                    }
                });
            });
            @endcan

            var table = $('#grid-sertipikat').DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                PaginationType: "two_button",
                paginate:true,
                deferRender: true,
                keys: false,
                dom: '<"row"<"col-md-4"l><"col-md-4 text-center"B><"col-md-4"f>>' + 'rtip',
                buttons: [
                    {
                        text: '<i class="fa fa-refresh"></i> Refresh',
                        className: "btn-sm",
                        action: function(e, dt, node, config) {
                            dt.ajax.reload(null, false);
                        }
                    },
                ],
                ajax: { 
                    url: "{!! route('sertipikat.index.data') !!}",
                    pages: 10,
                },
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', className: "small", orderable: false, searchable: false},
                    {data: 'jenis', name: 'jenis', className: "small"},
                    {data: 'tipe', name: 'tipe', className: "small"},
                    {data: 'nomor', name: 'nomor', className: "small"},
                    {data: 'pemegang_hak', name: 'pemegang_hak', className: "small"},
                    {data: 'luas', name: 'luas', className: "small text-right"},
                    {data: 'alamat', name: 'alamat', className: "small"},
                    {data: 'tgl_buat', name: 'tgl_buat', className: "small"},
                    {data: 'tgl_akhir', name: 'tgl_akhir', className: "small"},
                    {data: 'bus_unit', name: 'bus_unit', className: "small"},
                    {data: 'updated_at', name: 'updated_at', className: "small"},
                    {data: 'action', name: 'action', className: "small text-center", orderable: false, searchable: false},
                ],
                columnDefs: [
                    {targets: 5, render: $.fn.dataTable.render.number( '.', ',', 0, '' )},
                    {targets: 7, render: $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD-MM-YYYY' )},
                    {targets: 8, render: $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD-MM-YYYY' )},
                    {targets: 10, render: $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD-MM-YYYY HH:mm' )},
                ],
                order: [
                    [10, 'desc']
                ],
                language: {
                    processing: "<p class='text-center'>Loading...</p>"
                },
            });
        });

        function showDetail(id) {
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{!! route('sertipikat.show') !!}",
                data: {id: id},
                success: function(data) {
                    $('#form-modal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#content').html(data);
                }
            });
        }

        function showSearch() {
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{!! route('sertipikat.search') !!}",
                success: function(data) {
                    $('#form-modal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#content').html(data);
                }
            });
        }

    </script>
@endsection