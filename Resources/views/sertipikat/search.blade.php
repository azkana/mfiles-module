<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="detail">Search {!! $title !!} on M-Files</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="q" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-8">
                    {!! Form::text('q', null, [ 'class' => 'form-control', 'id' => 'q', 'autocomplete' => 'off']) !!}
                </div>
                <div class="col-sm-2">
                    <a class="btn btn-default" onclick="searchNow(); return false;">
                        <i class="fa fa-search"></i> Search
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="content-data"></div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-success" id="btn-sync-now" style="display: none" onclick="syncNow(); return false;"><i class="fa fa-refresh"></i> Sync</button>
</div>

<script type="text/javascript">
    $(document).ready(function() {

    });

    function searchNow() {
        var q = $('#q').val();
        $.ajax({
            type: "GET",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{!! route('sertipikat.search.now') !!}",
            data: {q: q},
            success: function(data) {
                $('#content-data').html(data);
                $('#btn-sync-now').show();
            }
        });
    }

    function syncNow() {
        var q = $('#q').val();
        $.ajax({
            type: "GET",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{!! route('sertipikat.sync.now') !!}",
            data: {q: q},
            success: function(data) {
                toastr.success(data.message);
                $('#grid-sertipikat').DataTable().ajax.reload(null, false);
            }
        });
    }
</script>