<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="detail">Detail {!! $title !!}</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <table class="table table-condensed table-striped">
                <tbody>
                    <tr>
                        <th style="width: 40%">Jenis Dokumen</th>
                        <td>{!! $data->jenis !!}</td>
                    </tr>
                    <tr>
                        <th>Tipe</th>
                        <td>{!! uppercase($data->tipe) !!}</td>
                    </tr>
                    <tr>
                        <th>Nomor</th>
                        <td>{!! $data->nomor !!}</td>
                    </tr>
                    <tr>
                        <th>Pemegang Hak</th>
                        <td>{!! $data->pemegang_hak !!}</td>
                    </tr>
                    <tr>
                        <th>Business Unit</th>
                        <td>{!! $data->bus_unit !!}</td>
                    </tr>
                    <tr>
                        <th>Luas</th>
                        <td>{!! numberFormat($data->luas) !!} m<sup>2</sup></td>
                    </tr>
                    <tr>
                        <th>Alamat</th>
                        <td>{!! $data->alamat !!}</td>
                    </tr>
                    <tr>
                        <th>Tgl. Sertipikat</th>
                        <td>{!! dateFormatDmy($data->tgl_buat) !!}</td>
                    </tr>
                    <tr>
                        <th>Tgl. Valid</th>
                        <td>{!! dateFormatDmy($data->tgl_akhir) !!}</td>
                    </tr>
                    <tr>
                        <th>Doos</th>
                        <td>{!! $data->doos !!}</td>
                    </tr>
                    <tr>
                        <th>Rak</th>
                        <td>{!! $data->rak !!}</td>
                    </tr>
                    <tr>
                        <th>BAST</th>
                        <td>{!! $data->bast !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
            <table class="table table-condensed table-striped">
                <tbody>
                    <tr>
                        <th style="width: 50%">ID Dokumen</th>
                        <td>{!! $data->doc_id !!}</td>
                    </tr>
                    <tr>
                        <th>Title Dokumen</th>
                        <td>{!! $data->doc_title !!}</td>
                    </tr>
                    <tr>
                        <th>Versi Dokumen</th>
                        <td>{!! $data->doc_version !!}</td>
                    </tr>
                    <tr>
                        <th>Tipe Dokumen</th>
                        <td>{!! $data->file_ext !!}</td>
                    </tr>
                    <tr>
                        <th>Ukuran Dokumen</th>
                        <td>{!! formatSizeUnits($data->file_size) !!}</td>
                    </tr>
                    
                    <tr>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Created</th>
                        <td>{!! dateFormatDmyHi($data->created) !!} WIB</td>
                    </tr>
                    <tr>
                        <th>Created By</th>
                        <td>{!! capitalize($data->created_by) !!}</td>
                    </tr>
                    <tr>
                        <th>Last Modified</th>
                        <td>{!! dateFormatDmyHi($data->last_modified) !!} WIB</td>
                    </tr>
                    <tr>
                        <th>Last Modified By</th>
                        <td>{!! capitalize($data->last_modified_by) !!}</td>
                    </tr>
                    <tr>
                        <th>Last Sync</th>
                        <td>{!! dateFormatDmyHi($data->last_sync) !!} WIB</td>
                    </tr>
                    <tr>
                        <th>Last Sync By</th>
                        <td>{!! capitalize($data->lastSyncBy->name) !!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-condensed table-striped">
                <thead>
                    <tr>
                        <th style="width: 50%">Supporting Dokumen:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            @if(!empty($data->sup_doc))
                                <ol>
                                    @foreach (explode(';', $data->sup_doc) as $row)
                                        <li>{!! $row !!}</li>
                                    @endforeach
                                </ol>
                            @else
                                <span class="text-red">tidak ada supporting dokumen</span>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal-footer"></div>