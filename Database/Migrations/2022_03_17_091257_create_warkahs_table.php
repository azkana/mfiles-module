<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarkahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mfl_warkah', function (Blueprint $table) {
            $table->uuid('id');
            $table->bigInteger('index')->nullable();
            $table->string('kode', 10)->nullable();
            $table->string('nomor', 50);
            $table->string('name', 100);
            $table->string('jenis', 50);
            $table->double('luas')->default(0);
            $table->string('desa', 100);
            $table->string('kecamatan', 100);
            $table->string('kabupaten', 100);
            $table->string('sph_no', 50);
            $table->string('sph_nm', 100)->comment('Nama SPH');
            $table->date('sph_dt');
            $table->double('sph_ls')->default(0)->comment('Luas SPH');
            $table->boolean('ktp')->default(false);
            $table->boolean('kk')->default(false);
            $table->boolean('sk_sengketa')->default(false)->comment('Surat Keterangan Sengketa');
            $table->boolean('sk_waris')->default(false)->comment('Surat Keterangan Waris');
            $table->boolean('k_waris')->default(false)->comment('Kuasa Waris');
            $table->boolean('k_jual')->default(false)->comment('Kuasa Jual');
            $table->boolean('pbb')->default(false);
            $table->dateTime('last_sync')->nullable();
            $table->integer('last_sync_by')->unsigned()->nullable();
            $table->string('doc_id', 10)->nullable();
            $table->string('doc_title', 200)->nullable();
            $table->string('doc_version', 3)->nullable();
            $table->string('doc_type', 3)->nullable();
            $table->double('file_size')->default(0);
            $table->string('file_ext', 10)->nullable();
            $table->timestamp('created')->nullable();
            $table->string('created_by', 150)->nullable();
            $table->timestamp('last_modified')->nullable();
            $table->string('last_modified_by', 150)->nullable();
            $table->timestamps();

            $table->primary('id');
            $table->index('index');
            $table->index('kode');
            $table->index('nomor');
            $table->index('name');
            $table->index('jenis');
            $table->index('desa');
            $table->index('kecamatan');
            $table->index('kabupaten');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mfl_warkah');
    }
}
