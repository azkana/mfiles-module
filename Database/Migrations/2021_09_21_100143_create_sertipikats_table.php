<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSertipikatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mfl_sertipikat', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->bigInteger('index');
            $table->string('kode', 7)->nullable();
            $table->string('nomor', 5)->nullable();
            $table->date('tgl_buat')->nullable();
            $table->date('tgl_akhir')->nullable();
            $table->string('jenis', 10)->nullable();
            $table->string('tipe', 20)->nullable();
            $table->double('luas')->default(0);
            $table->string('alamat', 200)->nullable();
            $table->string('pemegang_hak', 50)->nullable();
            $table->string('bus_unit', 50)->nullable();
            $table->string('doos', 50)->nullable();
            $table->string('rak', 5)->nullable();
            $table->string('bast', 100)->nullable();
            $table->text('sup_doc', 1000)->nullable();
            $table->dateTime('last_sync')->nullable();
            $table->integer('last_sync_by')->unsigned()->nullable();
            $table->string('doc_id', 10)->nullable();
            $table->string('doc_title', 200)->nullable();
            $table->string('doc_version', 3)->nullable();
            $table->string('doc_type', 3)->nullable();
            $table->double('file_size')->default(0);
            $table->string('file_ext', 10)->nullable();
            $table->timestamp('created')->nullable();
            $table->string('created_by', 150)->nullable();
            $table->timestamp('last_modified')->nullable();
            $table->string('last_modified_by', 150)->nullable();
            $table->timestamps();

            $table->index('index');
            $table->index('kode');
            $table->index('nomor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mfl_sertipikat');
    }
}
