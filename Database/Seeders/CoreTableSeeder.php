<?php

namespace Modules\Mfiles\Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class CoreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $permissions = [
            'mfiles',
            'mfiles-dashboard-read',
            'mfiles-sertipikat-read', 'mfiles-sertipikat-sync', 'mfiles-sertipikat-export', 'mfiles-sertipikat-search',
            'mfiles-warkah-read', 'mfiles-warkah-sync', 'mfiles-warkah-export',
        ];

        foreach($permissions as $row) {
            $permission = Permission::where('name', $row)->first();
            if($permission) {
                $this->command->info('Permission ' .$row. ' already exists.');
            } else {
                Permission::create([
                    'name' => $row
                ]);
                $this->command->info('Permission ' .$row. ' created successfully.');
            }
        }

        $roles = ['mpk-user'];

        foreach($roles as $row) {
            $role = Role::where('name', $row)->first();
            if($role) {
                $this->command->info('Role ' .$row. ' already exists.');
                $thisRole = $role;
            } else {
                $newRole = Role::create([
                    'name' => $row
                ]);
                $this->command->info('Role ' .$row. ' created successfully.');
                $thisRole = $newRole;
            }

            switch($row) {
                case 'mpk-user':
                    $thisRole->givePermissionTo($permissions);
                    break;
            }
            $this->command->info('Role ' . $row .' syncronized with permissions successfully.');
        }

        /**
         * Create default user
         */

        $users = [
            ['name' => 'MPK User', 'email' => 'mpk_user@demo.com', 'role' => 'mpk-user'],
        ];

        foreach($users as $row) {
            
            $user = User::where('email', $row['email'])->first();
            
            if($user) {
                
                $this->command->info('User ' . $row['name'] . ' already exists.');
            
            } else {
                
                $newUser = User::create([
                    'name' => $row['name'],
                    'email' => $row['email'],
                    'password' => Hash::make('secret'),
                    'email_verified_at' => Carbon::now(),
                    'is_active' => true,
                ]);

                $newUser->assignRole($row['role']);

                $this->command->info('User ' . $row['name'] . ' with role ' . $row['role'] . ' created successfully.');
            
            }
            
            $this->command->info(' All User granted successfully.');
        }

    }
}
